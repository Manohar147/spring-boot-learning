package com.muraliveeravalli.springbootdemo.springbootlearning1.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class FunRestController {

    @GetMapping("/")
    public String sayHello() {
        return "Hello World 123! Time in server is: " + LocalDateTime.now();
    }

    @GetMapping("/workout")
    public String getDailyWorkout() {
        return "Run a  hard 5K! Time in server is: " + LocalDateTime.now();
    }

    @GetMapping("/fortune")
    public String getDailyFortune() {
        return "Today is your lucky day Yohoo! Time in server is: " + LocalDateTime.now();
    }
}
